# Laboratory equipment management

Poor Man's lab equipment management system.
Just for learning and practicing things.

Because I'm just learning some stuff in my spare time, do not consider this
as well-written production code, but feel free to read, get some ideas or
comment. :wink:

## Roadmap

Probably, it will be updated from time to time.

- prepare DB
    - info about
        - items
        - containers
        - item tags
    - initial ops
        - cli
            - show contents
            - add/del items, containers, tags
    - model
        - SQL Alchemy
    - location
        - in file (for the beginning/tests)

- think about DB location?
- host the DB
- simple web app for accessing DB and modifying items
- item data retrieval using QR codes on boxes
- support item photos
- web app with "near real-time" cam processing
    - printing item info on the screen
    - printing item preview on the screen


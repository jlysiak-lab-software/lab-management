from unittest import TestCase
from sqlalchemy.orm import sessionmaker

from equipment.simple_db_interface import SimpleDbInterface
from equipment.models import Tag

from .db_test_helpers import init_test_db


class SimpleDbInterfaceTest( TestCase ):

    def setUp( self ):
        self.engine = init_test_db()
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

    def tearDown( self ):
        self.session.close()
        self.engine.dispose()

    def test_getTagById( self ):
        sif = SimpleDbInterface( self.session )
        tags = sif.getTags( ids=[0] )
        self.assertEqual( len( tags ), 1, "Got not exactly one tag" )
        tag = tags[ 0 ]
        self.assertEqual( tag.name, "A", "Got wrong tag name!" )

    def test_getTagsById( self ):
        sif = SimpleDbInterface( self.session )
        tags = sif.getTags( ids=[1, 2] )
        self.assertEqual( len( tags ), 2, "Got not exactly two tags" )
        self.assertTrue( all( map( lambda x: type(x) is Tag, tags ) ),
                         "Not Tags" )
        tags.sort( key=lambda x: x.tagId )
        for tag, name in zip(tags, ['B', 'C']):
            self.assertEqual( tag.name, name, "Got wrong tag name!" )

    def test_getTagByName( self ):
        sif = SimpleDbInterface( self.session )
        tags = sif.getTags( names=['A'] )
        self.assertEqual( len( tags ), 1, "Got not exactly one tag" )
        self.assertTrue( all( map( lambda x: type(x) is Tag, tags ) ),
                         "Not Tags" )
        self.assertEqual( tags[0].tagId, 0, "Got wrong tagId!" )

    def test_getTagsByName( self ):
        sif = SimpleDbInterface( self.session )
        tags = sif.getTags( names=['A', 'C'] )
        self._isOfLen( tags, 2 )
        self._areOfType( tags, Tag )

        tags.sort( key=lambda x: x.tagId )
        for tag, name, tagId in zip( tags, ['A', 'C'], [ 0, 2 ] ):
            self.assertEqual( tag.name, name, "Got wrong tag name!" )
            self.assertEqual( tag.tagId, tagId, "Got wrong tag ID!" )

    def _areOfType( self, xs, cls):
        self.assertTrue( all( map( lambda x: type(x) is cls, xs ) ),
                         "Not " + cls.__name__ )

    def _isOfLen( self, xs, n):
        self.assertEqual( len( xs ), n, "Got not exactly %d elements" % n )

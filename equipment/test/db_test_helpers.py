from importlib import resources
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker
from logging import getLogger

from equipment.models import Item, Tag, Container, Base

logger = getLogger(__name__)

def is_db_empty( engine ):
    inspector = inspect( engine )
    return inspector.get_table_names() == []

def load_test_data( txt_file ):
    with open( txt_file ) as f:
        data = []
        for line in f:
            data += [ tuple( line.strip().split(';') ) ]
        return data

def get_test_containers( data ):
    cs = {}
    for idStr, label in data[1:]:
        c = Container()
        c.containerId = int(idStr)
        c.label = label
        cs[ c.containerId ] = c
    return cs

def get_test_tags( data ):
    ts = {}
    for idStr, name in data[1:]:
        t = Tag()
        t.tagId = int(idStr)
        t.name = name
        ts[ t.tagId ] = t
    return ts

def get_test_items( data, containers, tags ):
    xs = []
    for idStr, name, desc, qty, cs, ts in data[1:]:
        x = Item()
        x.itemId = int(idStr)
        x.name = name
        x.description = desc
        x.quantity = int(qty)
        x.containers = \
            list( map( lambda x: containers[ int( x ) ], cs.split(':') ) )
        x.tags = list( map( lambda x: tags[ int( x ) ], ts.split(':') ) )
        xs += [ x ]
    return xs

def init_test_db():
    # Create in-memory db
    engine_path = 'sqlite://'
    logger.debug( 'Creating in-memory db engine: %s', engine_path )
    engine = create_engine( engine_path, echo=False )
    logger.debug( "Initializing db..." )

    Base.metadata.create_all( engine )

    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()

    with resources.path( 'equipment.data', 'containers.txt' ) as txt_file:
        data = load_test_data( txt_file )
        containers = get_test_containers( data )
    for c in containers.values():
        session.add( c )

    with resources.path( 'equipment.data', 'tags.txt' ) as txt_file:
        data = load_test_data( txt_file )
        tags = get_test_tags( data )

    for t in tags.values():
        session.add( t )
    session.commit()

    with resources.path( 'equipment.data', 'items.txt' ) as txt_file:
        data = load_test_data( txt_file )
        items = get_test_items( data, containers, tags )
    for x in items:
        session.add( x )
    session.commit()
    return engine

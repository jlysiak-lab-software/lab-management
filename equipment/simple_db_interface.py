from .models import Container, Item, Tag

class SimpleDbInterface:

    def __init__( self, session ):
        self.session = session

    def getContainers( self, ids=None, labels=None ):
        query = self.session.query( Container )
        if ids:
            assert type( ids ) in [ list, tuple ]
            query = query.filter( Container.containerId.in_( ids ) )
        if labels:
            assert type( ids ) in [ list, tuple ], "Not a list or tuple"
            assert all( map( lambda x: type( x ) is str, labels ) ), \
                   "Not all labels are strings"
            query = query.filter( Container.label.in_( labels ) )
        return query.all()

    def getItems( self, ids=None ):
        query = self.session.query( Item )
        if ids:
            assert type( ids ) in [ list, tuple ], "ids: Not a list or tuple"
            query = query.filter( Container.containerId.in_( ids ) )
        return query.all()

    def getTags( self, ids=None, names=None, nameRegEx=None ):
        query = self.session.query( Tag )
        if ids:
            assert type( ids ) in [ list, tuple ]
            query = query.filter( Tag.tagId.in_( ids ) )
        if names:
            assert type( names ) in [ list, tuple ]
            assert all( map( lambda x: type( x ) is str, names ) ), \
                   "Not all names are strings"
            query = query.filter( Tag.name.in_( names ) )
        if nameRegEx:
            assert type( nameRegEx ) is str
            assert False, "Not supported!"
        return query.all()

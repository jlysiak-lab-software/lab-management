import argparse

class CliParser:

    def __init__( self ):
        self.parser = argparse.ArgumentParser()

    def parse( self ):
        self.parser.parse_args()

# Lab equipment database model

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.orm import relationship

Base = declarative_base()

# === Many-to-many relationships
item_container = Table( "item_container",
                        Base.metadata,
                        Column( "itemId", Integer,
                                ForeignKey( "item.itemId" ) ),
                        Column( "containerId", Integer,
                                ForeignKey( "container.containerId" ) ) )

item_tag = Table( "item_tag",
                  Base.metadata,
                  Column( "itemId", Integer,
                          ForeignKey( "item.itemId" ) ),
                  Column( "tagId", Integer,
                          ForeignKey( "tag.tagId" ) ) )

# === Models

class Container( Base ):
    '''Container models a base storage unit.
        Every unit in the lab has its own label.
    '''
    __tablename__ = "container"
    containerId = Column( Integer, primary_key=True )
    label = Column( String )
    items = relationship( "Item",
                          # use 'item_container' table to model *-* relation
                          secondary=item_container,
                          back_populates="containers" )

    def __str__( self ):
        items = str([x.itemId for x in self.items])
        return f'Container(id={self.containerId}, label={self.label}, items={items})'

class Item( Base ):
    '''Item models just some kind of item/s.'''
    __tablename__ = "item"
    itemId = Column( Integer, primary_key=True )
    name = Column( String )
    description = Column( String )
    quantity = Column( Integer )
    containers = relationship( "Container",
                               secondary=item_container,
                               back_populates="items" )
    tags = relationship( "Tag",
                         secondary=item_tag,
                         back_populates="items" )
    def __str__( self ):
        return f'Item(id={self.itemId}, name={self.name}, desc={self.description})'

class Tag( Base ):
    __tablename__ = "tag"
    tagId = Column( Integer, primary_key=True )
    name = Column( String )
    items = relationship( "Item",
                          secondary=item_tag,
                          back_populates="tags" )

    def __str__( self ):
        items = str([x.itemId for x in self.items])
        return f'Tag(id={self.tagId}, label={self.name}, items={items})'

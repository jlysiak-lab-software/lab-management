#!/bin/sh

# Copyright (C) 2021, Jacek Łysiak
#
# Poor Man's unit tests runner.
# Just watch for file modifications and run all tests some file
# has been saved.
# I really recommend to go through `man flock` briefly, but I will try
# to give some explanations.


LOCK=.test_runner_lock

function cleanup {
    echo "Cleaning up..."
    # Obtain exclusive lock and drop it before executing `rm`
    flock $LOCK rm $LOCK
}

trap cleanup EXIT

while read LINE
do
    # Try to obtain exclusive access to lock-file in non-blocking manner ( -n )
    # If we got an exclusive access, we succeed and run `make test`.
    # Otherwise, `flock -n 9` fails and sub-shell (only!) exits.
    # We, of course, run sub-shell in background, so we are not blocking the
    # main loop.
    ( flock -n 9 || exit 1; printf "\n\n"; make test ) 9> $LOCK &

done < <(inotifywait -m -r equipment -q -e close_write --format '%w%f' )

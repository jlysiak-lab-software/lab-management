from setuptools import setup

setup(
    name='lab',
    version='0.1',
    py_modules=[ 'equipment' ],
    install_requires=[
        'sqlalchemy',
        'flask'
    ],
    entry_points={
        'console_scripts': [ 'eq = equipment:main' ],  # DB cli access
    },
)

test:
	python -m unittest discover

testrunner:
	./testrunner.sh

clean:
	rm -rf *.egg-info

.PHONY: clean test testrunner
